'use strict';

angular.module('app.theme', []);

angular.module('app.theme')
.config(['$mdThemingProvider', function ($mdThemingProvider) {

        //Angular Material Theme Configuration
        $mdThemingProvider.theme('default')
            .primaryPalette('{{primaryColor}}')
            .accentPalette('{{secondaryColor}}')
            .warnPalette('{{warnColor}}');

        $mdThemingProvider.theme('docs-dark', 'default')
            .primaryPalette('{{secondaryColor}}')
            .accentPalette('{{contrastSecondaryColor}}')
            .warnPalette('{{contrastWarnColor}}')
            .dark();

    }]);